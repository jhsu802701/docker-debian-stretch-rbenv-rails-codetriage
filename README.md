# Docker Debian Stretch - rbenv - Rails - CodeTriage

This repository is used for building a custom Docker image for [CodeTriage](https://github.com/codetriage/codetriage).

## Name of This Docker Image
[registry.gitlab.com/jhsu802701/docker-debian-stretch-rbenv-rails-codetriage](https://gitlab.com/jhsu802701/docker-debian-stretch-rbenv-rails-codetriage/container_registry)

## Upstream Docker Image
[registry.gitlab.com/rubyonracetracks/docker-debian-stretch-min-rbenv](https://gitlab.com/rubyonracetracks/docker-debian-stretch-min-rbenv/container_registry)

## What's Added
* The latest versions of the rails, pg, nokogiri, and ffi gems
* The versions of the above gems used in the CodeTriage app
* The mailcatcher gem
* The correct Ruby version WITH the above gems plus the Ruby version to upgrade to

## More Information
General information common to all Docker Debian build repositories is in the [FAQ](https://gitlab.com/rubyonracetracks/docker-debian-common/blob/master/FAQ.md).
