#!/bin/bash

export ABBREV='rbenv-rails-codetriage'
export OWNER='jhsu802701'
export BRANCH='stretch'
export DISTRO='debian'
export DOCKER_IMAGE="registry.gitlab.com/$OWNER/docker-$DISTRO-$BRANCH-$ABBREV"
export DOCKER_CONTAINER="container-$ABBREV"
